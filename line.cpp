#include "line.h"
#include "vector2.h"

namespace clusterize_test
{
	template<Scalar scalar, size_t dim>
	line<scalar, dim>::line(vector<scalar, dim> st, vector<scalar, dim> fin) noexcept : st_(std::move(st)), fin_(std::move(fin))
	{};

	template<Scalar scalar, size_t dim>
	vector<scalar, dim> line<scalar, dim>::dir() const
	{
		auto d = fin_ - st_;
		if (auto len2 = abs2(d); len2 != 0.f)
			return d / sqrt(len2);
		else
			return {};
	}

	//template<Scalar scalar>
	//std::optional<vector<scalar, 2>> intersection(const line<scalar, 2>& l, const line<scalar, 2>& r)
	//{
	//	const auto& d1 = l.fin_ - l.st_;
	//	const auto& d2 = r.fin_ - r.st_;

	//	const scalar& det = d1.x * d2.y - d2.x * d1.y;

	//	if (abs(det) < 1e-6)
	//		return std::nullopt;

	//	const auto& stdiff = l.st_ - r.st_;
	//	const scalar& b1 = stdiff.x, b2 = stdiff.y;

	//	const scalar& t1 = (-b1 * d2.y + b2 * d2.x) / det;
	//	const scalar& t2 = (b2 * d1.x - b1 * d2.y) / det;

	//	return l.st + t1 * d1;
	//}

	template struct line<float, 2>;
	template struct line<double, 2>;
}