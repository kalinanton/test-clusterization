#include <iostream>
#include <filesystem>
#include <fstream>
#include <numeric>

#include "vector2.h"
#include "line.h"

#include <Eigen/Sparse>

#include <tbb/parallel_for.h>
#include <tbb/parallel_sort.h>
#include <tbb/enumerable_thread_specific.h>

#include <json/json.h>

#undef max
#undef min

using namespace clusterize_test;

int main(int argc, char* argv[])
{
	if (argc != 2)
	{
		std::cout << "Path to input must be only argument\n";
		return 1;
	}

	//SETTINGS
	float MAX_DIST = 10.f;

	float MAX_X = 2000.f;
	float MAX_Y = 2000.f;

	float MIN_X = -2000.f;
	float MIN_Y = -2000.f;

	//SETTINGS END

	std::filesystem::path path = argv[1];

	std::ifstream input(path);

	size_t m, n;
	input >> m >> n;

	struct line_info
	{
		std::optional<line<float, 2>> line_;
		std::optional<vector2<float>> point_;
	};

	std::vector<line_info> info_vec(m);

	for (auto& info : info_vec)
	{
		vector2<float> st, fin;
		input >> st >> fin;

		info.line_.emplace(st, fin);
	}

	std::vector<std::optional<vector2<float>>> points;
	points.reserve(m * m);


	for (auto it1 = begin(info_vec); it1 != end(info_vec); it1++)
	{
		for (auto it2 = it1 + 1; it2 != end(info_vec); it2++)
		{
			bool same = false;
			auto pt = intersection(it1->line_.value(), it2->line_.value(), same);

			if (pt && pt->x < MAX_X && pt->x > MIN_X && pt->y < MAX_Y && pt->y > MIN_Y)
				points.emplace_back(std::move(pt));
		}
	}

	std::vector<std::pair<vector2<float>, size_t>> found_points;

	tbb::enumerable_thread_specific<std::vector<Eigen::Triplet<float, size_t>>> triplets_tls;


	// Can be optimized: for example we can build AABB (or something) map and calculate dist only between close points, will be huge speedup
	tbb::parallel_for(tbb::blocked_range<size_t>(0, points.size()), [&](const tbb::blocked_range<size_t>& r)
	{
		auto& triplets = triplets_tls.local();

		for (size_t i = r.begin(); i < r.end(); i++)
			for (size_t i1 = i + 1; i1 < points.size(); i1++)
				if (auto dist = abs(points[i].value() - points[i1].value()); dist < MAX_DIST / 2.f)
				{
					triplets.emplace_back(i, i1, 1.f / std::max(dist, 1e-5f));
					triplets.emplace_back(i1, i, 1.f / std::max(dist, 1e-5f));
				}
	});

	auto triplets = triplets_tls.combine([](auto&& l, auto&& r) { std::copy(begin(r), end(r), std::back_inserter(l)); return std::forward< std::vector<Eigen::Triplet<float, size_t>>>(l); });

	Eigen::SparseMatrix<float> connectivity(points.size(), points.size());
	connectivity.setFromTriplets(begin(triplets), end(triplets));

	std::vector<std::pair<int, std::optional<float>>> descr(points.size());

	std::generate(begin(descr), end(descr), [i = 0]() mutable { return i++; });

	tbb::parallel_sort(rbegin(descr), rend(descr), [&](auto& l, auto& r) {
		if (!l.second)
			l.second = connectivity.col(l.first).norm();

		if (!r.second)
			r.second = connvectivity.col(l.first).norm();

		return l.second < r.second;
	});

	for (const auto& [desc, cache] : descr)
	{
		if (!points[desc])
			continue;

		auto ptdescr = points[desc].value();

		vector2<float> av = ptdescr;
		size_t count = 1;

		for (auto i = 0; i < points.size(); i++)
		{
			auto& opt = points[i];

			if (!opt)
				continue;

			if (abs(opt.value() - ptdescr) < MAX_DIST / 2.f)
			{
				av = av + opt.value();
				count++;
				opt.reset();
			}
		}

		found_points.emplace_back(av / count, count);

		if (found_points.size() == n + 5)
			break;
	}

	std::ofstream output("output.txt");

	std::sort(rbegin(found_points), rend(found_points), [](const auto& l, const auto& r) { return l.second < r.second; });

	for (const auto& pt : found_points)
		output << pt.first << " " << pt.second << "\n";

	return 0;
}

