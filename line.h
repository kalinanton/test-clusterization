#pragma once

#include <optional>
#include "math_misc.h"

namespace clusterize_test
{
	template<Scalar scalar, size_t dim>
	struct line
	{
		using vec = vector<scalar, dim>;

		[[no_unique_address]] vec st_, fin_;

		line() = default;
		line(const line&) = default;
		line(line&&) = default;

		line(vec st, vec fin) noexcept;

		vec dir() const;
	};

	template<Scalar scalar, size_t dim>
	std::optional<vector<scalar, dim>> intersection(const line<scalar, dim>&, const line<scalar, dim>&, bool&);

	template<Scalar scalar>
	std::optional<vector<scalar, 2>> intersection(const line<scalar, 2>& l, const line<scalar, 2>& r, bool& same)
	{
		const auto& d1 = l.fin_ - l.st_;
		const auto& d2 = r.fin_ - r.st_;

		const scalar& det = d1.x * d2.y - d2.x * d1.y;

		const auto& stdiff = l.st_ - r.st_;
		const scalar& b1 = stdiff.x, b2 = stdiff.y;

		if (std::abs(det) < 1e-6)
		{
			if (std::abs(-b1 * d2.y + b2 * d2.x) < 1e-6 && std::abs(b2 * d1.x - b1 * d1.y) < 1e-6)
				same = true;

			return std::nullopt;
		}

		const scalar& t1 = (-b1 * d2.y + b2 * d2.x) / det;
		const scalar& t2 = (b2 * d1.x - b1 * d1.y) / det;

		return l.st_ + t1 * d1;
	}
}