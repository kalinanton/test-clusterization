#pragma once

#include "math_misc.h"

#include <tuple>

namespace clusterize_test
{
	template<Scalar scalar>
	struct vector<scalar, 2>
	{
		[[no_unique_address]] scalar x, y;
	};

	template<size_t I, clusterize_test::Scalar T>
	constexpr const T& get(const clusterize_test::vector<T, 2>& v)
	{
		if constexpr (I == 0)
			return v.x;

		if constexpr (I == 1)
			return v.y;
	}

	template<size_t I, clusterize_test::Scalar T>
	constexpr T& get(clusterize_test::vector<T, 2>& v)
	{
		if constexpr (I == 0)
			return v.x;

		if constexpr (I == 1)
			return v.y;
	}

	template<Scalar scalar>
	using vector2 = vector<scalar, 2>;
}

namespace std
{
	template<size_t I, clusterize_test::Scalar T>
	struct tuple_element<I, clusterize_test::vector<T, 2>> { using type = T; };

	template< clusterize_test::Scalar T >
	struct tuple_size<clusterize_test::vector<T, 2>> : integral_constant<size_t, 2> {};
}