#pragma once

#include <concepts>
#include <iostream>

namespace clusterize_test
{
	template<typename T> concept Scalar = requires (T a, T b)
	{
		{ a + b } -> std::convertible_to<T>;
		{ a - b } -> std::convertible_to<T>;
		{ a* b } -> std::convertible_to<T>;
		{ a / b } -> std::convertible_to<T>;
		{ abs(a) } -> std::convertible_to<float>;
	};

	template<Scalar scalar, size_t dim>
	struct vector;

	template<Scalar scalar, size_t dim>
	vector<scalar, dim> operator+ (const vector<scalar, dim>& l, const vector<scalar, dim>& r)
	{
		return[&]<int ... seq>(std::integer_sequence<int, seq...>) -> vector<scalar, dim>
		{
			vector<scalar, dim> result;

			((get<seq>(result) = get<seq>(l) + get<seq>(r)), ...);
			return result;
		}(std::make_integer_sequence<int, dim>{});
	};

	template<Scalar scalar, size_t dim>
	vector<scalar, dim> operator- (const vector<scalar, dim>& l, const vector<scalar, dim>& r)
	{
		return[&]<int ... seq>(std::integer_sequence<int, seq...>) -> vector<scalar, dim>
		{
			vector<scalar, dim> result;

			((get<seq>(result) = get<seq>(l) - get<seq>(r)), ...);
			return result;
		}(std::make_integer_sequence<int, dim>{});
	};

	template<Scalar scalar1, std::convertible_to<scalar1> scalar2, size_t dim>
	vector<scalar1, dim> operator* (const vector<scalar1, dim>& v, const scalar2& a)
	{
		return[&]<int ... seq>(std::integer_sequence<int, seq...>) -> vector<scalar1, dim>
		{
			vector<scalar1, dim> result;

			((get<seq>(result) = static_cast<scalar1>(a) * get<seq>(v)), ...);
			return result;

		}(std::make_integer_sequence<int, dim>{});
	};

	template<Scalar scalar1, std::convertible_to<scalar1> scalar2, size_t dim>
	vector<scalar1, dim> operator* (const scalar2& a, const vector<scalar1, dim>& v)
	{
		return v * a;
	};

	template<Scalar scalar1, std::convertible_to<scalar1> scalar2, size_t dim>
	vector<scalar1, dim> operator/ (const vector<scalar1, dim>& v, const scalar2& a)
	{
		return[&]<int ... seq>(std::integer_sequence<int, seq...>) -> vector<scalar1, dim>
		{
			vector<scalar1, dim> result;

			((get<seq>(result) = get<seq>(v) / static_cast<scalar1>(a)), ...);
			return result;
		}(std::make_integer_sequence<int, dim>{});
	};

	template<Scalar scalar, size_t dim>
	scalar abs2(const vector<scalar, dim>& v)
	{
		constexpr auto sqr = [](auto&& x) { return x * x; };

		return[&]<int ... seq>(std::integer_sequence<int, seq...>) -> scalar
		{
			return (sqr(get<seq>(v)) + ...);

		}(std::make_integer_sequence<int, dim>{});
	}

	template<Scalar scalar, size_t dim>
	scalar abs(const vector<scalar, dim>& v)
	{
		return sqrt(abs2(v));
	}

	template<Scalar scalar, size_t dim>
	std::ostream& operator << (std::ostream& os, const vector<scalar, dim>& vec)
	{
		return[&]<int ... seq>(std::integer_sequence<int, seq...>) -> std::ostream&
		{
			(((os << get<seq>(vec)), (os << ' ')), ...);
			return os;
		}(std::make_integer_sequence<int, dim>{});
	}

	template<Scalar scalar, size_t dim>
	std::istream& operator >> (std::istream& is, vector<scalar, dim>& vec)
	{
		return[&]<int ... seq>(std::integer_sequence<int, seq...>) -> std::istream&
		{
			(is >> ... >> get<seq>(vec));
			return is;
		}(std::make_integer_sequence<int, dim>{});
	}

	template<Scalar scalar, size_t dim>
	scalar dot(const vector<scalar, dim>& l, const vector<scalar, dim>& r)
	{
		constexpr auto getMult = [](auto descr, auto&& l, auto&& r) { return get<descr()>(l) * get<descr()>(r); };

		return[&]<int ... seq>(std::integer_sequence<int, seq...>) -> scalar
		{
			return ((getMult(std::integral_constant<int, seq>{}, l, r)) + ...);

		}(std::make_integer_sequence<int, dim>{});
	}


};